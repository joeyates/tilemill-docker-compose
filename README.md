# tilemill-docker-compose

Dockerized tilemill with postgis.

Versions:

* OS: Ubuntu 18.04
* Tilemill: master
* PostgreSQL: 11
