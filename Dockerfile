FROM ubuntu:18.04

RUN apt-get update -qq
RUN apt-get install apt-transport-https
RUN apt-get install --assume-yes apt-utils curl gnupg2 git
RUN curl -sS https://deb.nodesource.com/gpgkey/nodesource.gpg.key \
  | apt-key add - 1>/dev/null 2>&1
RUN echo "deb https://deb.nodesource.com/node_8.x bionic main" \
  > /etc/apt/sources.list.d/nodesource.list
RUN apt-get update
RUN apt-get install \
    --assume-yes \
    nodejs

RUN mkdir /tilemill
WORKDIR /tilemill
RUN git clone https://github.com/tilemill-project/tilemill .
RUN npm install

CMD /tilemill/index.js start

EXPOSE 20009
EXPOSE 20008
